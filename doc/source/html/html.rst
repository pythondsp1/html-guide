HTML
****


Introduction
============

In this chapter, various component of HTML are discussed to design a web page. 

The basic structure for an HTML page is shown below.

* Entries inside the /< ... /> are known as tags. Most of the tags has an opening and closing e.g. \<head\> (opening head) and \</head\> (closing head). Some of the tags do not have closing tags e.g. \<!DOCTYPE ...\> and \<br /\>. We need to write the HTML codes inside the tags. 
* The comments are written between '<!--' and '-->'. 
* Here Line 1 gives the details of the 'HTML version' to the web-browser. The 'html' tells it is version 5. 
* The 'head' tag (Lines 3-5) contains the header related tags e.g. 'title for the page' and 'links for the css files' etc. 
* The 'body' tag (7-11) contains the actual HTML code which is displayed on the web-browser. Also, we add all the JavaScript related codes just before the closing body tag (\</body\>).  

.. code-block:: html
    :linenos:

    <!DOCTYPE html>  <!-- tells browser above the html version -->
    <html> <!-- beginning of the html document -->
        <head>
                <!-- header related tags e.g. title, links etc.  -->
        </head>

        <body>
                <!-- actual html document here -->

                <!-- add JavaScript files here -->
        </body>
    </html>



First code
==========

In below code, the message "Hello World" is displayed on the HTML page. The :numref:`fig_html1` is the resultant HTML page. 

* The title (Line 4) appears on the top of the browser. 
* The tag <h1> is called 'header' tag, which has the larger size than the normal text (see the size of 'Hello World!'). 
* The tag <p> is called the 'paragraph' tag, which can be used to write the paragraphs. 


.. code-block:: html

    <!DOCTYPE html>
    <html>
    <head>
        <title>HTML Tutorial</title>
    </head>
    <body>
        <h1> Hello World! </h1>
        <p> This is the first HTML code </p>
    </body>
    </html>


.. _`fig_html1`:

.. figure:: fig/html1.png

   First code


Basic tags
==========

* The :numref:`tbl_basic_tags` shows the list of tags which are required for writing the basic 'HTML' codes i.e. without any style e.g. bold, italics and numbering etc.  


.. _`tbl_basic_tags`:

.. table:: List of basic tags

    +-------------+--------------------------------------+-------------------------+
    | Tag         | Description                          | Example                 |
    +=============+======================================+=========================+
    | h1, ..., h6 | Header tag h1 to h6                  | <h2> Hi </h2>           |
    +-------------+--------------------------------------+-------------------------+
    | p           | paragraphs (Line changes at the end) | <p> Hi </p>             |
    +-------------+--------------------------------------+-------------------------+
    | span        | No line change after span            | <span>Hi</span> Bye.    |
    +-------------+--------------------------------------+-------------------------+
    | div         | make division between contents       | <div> ... </div>        |
    +-------------+--------------------------------------+-------------------------+
    | a           | hyperlink                            | see :numref:`sec_links` |
    +-------------+--------------------------------------+-------------------------+
    | center      | Move content to center               | <center> Hi </center>   |
    +-------------+--------------------------------------+-------------------------+
    | br          | Line break (no closing tag)          | <br /> or <br>          |
    +-------------+--------------------------------------+-------------------------+
    | hr          | horizontal line (no closing tag)     | <hr /> or <hr>          |
    +-------------+--------------------------------------+-------------------------+
    | pre         | preserve formatting                  | <pre> .... </pre>       |
    +-------------+--------------------------------------+-------------------------+
    | table       | insert table                         | see :numref:`sec_table` |
    +-------------+--------------------------------------+-------------------------+


* Let's see the example of each of these tags, 


.. note:: 

    All the new codes are added below the previous codes in the 'body' tag. Therefore only newly added codes are shown in the tutorial. 



.. code-block:: html

    <h2> Heading 2 </h2>
        <h6> Heading 6 </h6>

        <p> This is paragraph </p>

        <span> This is span.</span>
        <span> The 'br' tag is used after span to break the line </span> 
        <br/>

        <div style="color:blue;">  
            The 'div' tag can be used for formatting the tags inside it at once using 'style' and 'classes' etc. 

            <p> This paragraph is inside the 'div' tag </p>
            <span> This span is inside the 'div' tag </span>
            <br/>

        </div>

        <center>
            <h3> Heading 3 is centered</h3>
            <p><span> Centered span inside the paragraph.</span><p>
        </center>

        Two horizontal line is drawn using two 'hr' tag.
        <hr />
        <hr>


        <pre> 'pre' tag preserve the formatting (good for writing codes)

                # Python code
                x = 2
                y = 3
                print(x+y)

        </pre>


* :numref:`fig_html2` is the output of above code. Read the text to understand each tag, 


.. _`fig_html2`:

.. figure:: fig/html2.png

   Basic tags : Attribute 'style' is used in 'div' tag



Attributes
==========

In :numref:`fig_html2`, we saw an example of attribute (i.e. style) which changed the color of all the elements to 'blue' inside the 'div' tag. 

Attribute 'name' and 'value'
----------------------------

* Attribute is defined inside the opening part of a 'tag'. For example, in the below code, the attribute 'style' is defined inside the 'div' tag.

.. code-block:: html

    <div style="color:blue;"> 

    </div>

* An attribute has two parts i.e. '**name**' and '**value**'. For example, in the above code, **name** and **value** of the attribute are '**style**' and '**blue**' respectively. 


.. _`sec_core_attr`: 

Core attributes
---------------

Below are the three core attributes which are used frequently in web design. 

* **id** : The 'id' is the **unique** name which can be given to any tag. This is very useful in distinguishing the element with other elements. 

.. code-block:: html

    <p id='para1'> This is paragraph with id 'para1' </p>
    <p id='para2'> This is paragraph with id 'para2' </p>


* **class** : The attribute 'class' can be used with multiple tags. This is very useful in making groups in HTML design. 

.. code-block:: html

    <p class="c_blue"> This is paragraph with class 'blue'</p>
    <span class="c_blue"> This is span with class 'blue'</span>


* **style** : We already see the example of style attribute, which can be used to change the formatting of the text in HTML design. We can specify various styles which are discussed in :numref:`Chapter %s <ch_css>`. 
  
.. code-block:: html
 
    <p style="font-weight:bold; color:red;">Style attribute is used to bold and color</p>

.. note::

    Above three attributes are used with 'CSS (cascading style sheet)' and JavaScript/jQuery, which are the very handy tools to enhance the look and functionalities of the web-page respectively. The CSS is discussed in :numref:`Chapter %s <ch_css>`, whereas JavaScript and jQuery are discussed in :numref:`Chapter %s <ch_javascript>` and :numref:`Chapter %s <ch_jQuery>` respectively. 

* Also we can define multiple attributes for one tag as shown below, 
  
.. code-block:: html

    <p class="my_class" id="para_with_class" style="color:green"> Multiple attributes </p> 



* The other useful attributes are listed in :numref:`tbl_lst_of_attr`


.. _`tbl_lst_of_attr`:

.. table:: List of attributes 

    +-------------+---------------------------+--------------------------------------------------+
    | Name        | Values                    | Description                                      |
    +=============+===========================+==================================================+
    | id          | user defined names        | <p id='p_1'> Hi </p>                             |
    +-------------+---------------------------+--------------------------------------------------+
    | class       | user defined names        | <p class='p_class'> Hi </p>                      |
    +-------------+---------------------------+--------------------------------------------------+
    | style       | CSS styles                | <p style="color:red; font-weight:bold;"> Hi </p> |
    +-------------+---------------------------+--------------------------------------------------+
    | align       | left, right, center       | horizontal alignment                             |
    +-------------+---------------------------+--------------------------------------------------+
    | width       | numeric value or \% value | width of images and tables etc.                  |
    +-------------+---------------------------+--------------------------------------------------+
    | height      | numeric value             | height of images and tables etc.                 |
    +-------------+---------------------------+--------------------------------------------------+


.. _`sec_table`:

Tables
======

In this section, we will learn to draw tables along with some attributes which are discussed in :numref:`tbl_lst_of_attr`. :numref:`tbl_table_tag` shows the list of tags available to create the table, which are used in :numref:`html_ex_table`. 


.. _`tbl_table_tag`:

.. table:: Tags and attributes for creating tables

    +----------------+----------------------------------------+
    | Tag            | Description                            |
    +================+========================================+
    | table          | beginning and end of table             |
    +----------------+----------------------------------------+
    | tr             | row of table                           |
    +----------------+----------------------------------------+
    | th             | header cell                            |
    +----------------+----------------------------------------+
    | td             | data cell                              |
    +----------------+----------------------------------------+
    | **Attributes** |                                        |
    +----------------+----------------------------------------+
    | rowspan        | number of rows to merge                |
    +----------------+----------------------------------------+
    | colspan        | number of columns to merge             |
    +----------------+----------------------------------------+
    | border         | width of border                        |
    +----------------+----------------------------------------+
    | cellpadding    | width of whitespace between two border |
    +----------------+----------------------------------------+
    | cellspacing    | width of whitespace within a border    |
    +----------------+----------------------------------------+
    | bgcolor        | background color                       |
    +----------------+----------------------------------------+
    | bordercolor    | color of border                        |
    +----------------+----------------------------------------+
    | width          | width of table (numeric or \%)         |
    +----------------+----------------------------------------+
    | height         | height of table (numeric)              |
    +----------------+----------------------------------------+
    | caption        | caption for table                      |
    +----------------+----------------------------------------+


* Some of the attributes of :numref:`tbl_table_tag` are used in below example, 

.. code-block:: html
    :linenos:
    :caption: Table with border and color
    :name: html_ex_table

    <!-- border-color, width and height -->
    <table border="1" bordercolor="black" width="450" height="100">
    <caption>Table 1 : Various tags of table</caption>
        <tr bgcolor="red" > <!-- row -->
            <th>Column 1</th> <!-- header -->
            <th>Column 2</th>
            <th>Column 3</th>
        </tr>

        <tr bgcolor="cyan"> <!-- background color -->
            <td>Data 1</td> <!-- data -->
            <td>Data 2</td>
            <td>Data 3</td>
        </tr>

        <tr bgcolor="yellow"> <!-- row -->
            <td colspan="2">New Data 1</td> <!-- column span -->
            <td>New Data 2</td> <!-- data -->
        </tr>
    </table>



    <!-- width in % -->
    <table border="1" bordercolor="black" width="80%" height="100">
    <caption> Table 2 : Width is 80%</caption>
        <tr bgcolor="red" > 
            <th>Column 1</th> 
            <th>Column 2</th>
            <th>Column 3</th>
        </tr>

        <tr bgcolor="cyan"> <!-- row -->
            <td>Data 1</td> <!-- data -->
            <td>Data 2</td>
            <td>Data 3</td>
        </tr>

    </table>

* :numref:`fig_table_tag` is the output of above code, 

.. _`fig_table_tag`:

.. figure:: fig/html3.png

   Table generated by :numref:`tbl_table_tag`


Text formatting
===============

In this section, we will see some of the text formatting options (see :numref:`tbl_txt_format`) e.g. bold, italic, subscript and strike etc. 

.. _`tbl_txt_format`:

.. table:: Text formatting 

    +-------------+-----------------+
    | Tag         | Description     |
    +=============+=================+
    | b           | bold            |
    +-------------+-----------------+
    | i           | italic          |
    +-------------+-----------------+
    | u, ins      | underline       |
    +-------------+-----------------+
    | strike, del | strike          |
    +-------------+-----------------+
    | sup         | superscript     |
    +-------------+-----------------+
    | sub         | subscript       |
    +-------------+-----------------+
    | big         | big size text   |
    +-------------+-----------------+
    | small       | small size text |
    +-------------+-----------------+

* Below are the some of the examples of text formatting, whose results are shown in :numref:`fig_html4`, 

.. code-block:: html

    <!-- Text formatting  -->
    <p>This is <b>bold</b> text</p>
    <p>This is <strike>striked</strike> text</p>
    <p>This is <sub>subscript</sub> text</p>


.. _`fig_html4`:

.. figure:: fig/html4.png

   Text formatting 


Images
======

Image tag has two important attribues i.e. 'src' and 'alt' as described below, 

* **src** : tells the location of 'image' file e.g. in Line 2 the image 'logo.jpg' will be searched inside the folder 'img'. 
* **alt** : is the 'alternate text' which is displayed if image is not found. For example, in Line 6, the name of the image is incorrectly written i.e. 'logoa' (instead of 'logo'), therefore the value of 'alt' i.e. 'Missing Logo.jpg' will be displayed  as shown in :numref:`fig_html5`. 

.. code-block:: html
    :linenos:

    <!-- Images -->
    <img src="img/logo.jpg" alt="Logo.jpg" width="20%"/>
    
    <br/> <br/>
        
    <img src="img/logoa.jpg" alt="Missing Logo.jpg" width="20%"/>


.. _`fig_html5`:

.. figure:: fig/html5.png

   Images


.. note:: 

    We can use other attributes as well e.g. '**height**', '**align**' and '**border**' etc. 


Lists
=====

There are three type of lists in HTML, 

* Unordered list : bullet are used in it (see Lines 2 and 9)
* Ordered list : numbers are used in it (see Lines 15, 22 and 28)
* Definition list : This can be used for writing definitions in HTML (see Line 35)



.. code-block:: html
    :linenos:
    :emphasize-lines: 2, 9, 15, 22, 28, 35
    
    <!-- Lists -->
    <!-- unordered list -->
    <ul> Unordered List
        <li>Pen</li>
        <li>Pencil</li>
        <li>Eraser</li>
    </ul>

    <ul type="circle"> Change bullets : 'square', 'circle' or 'disc'
        <li>Pen</li>
        <li>Pencil</li>
        <li>Eraser</li>
    </ul>

    <!-- ordered list -->
    <ol> Ordered List
        <li>Pen</li>
        <li>Pencil</li>
        <li>Eraser</li>
    </ol>

    <ol type='i'> Change style : 'i', 'I', '1', 'a' or 'A'
        <li>Pen</li>
        <li>Pencil</li>
        <li>Eraser</li>
    </ol>

    <ol type='i' start="5"> Start from 'v'
        <li>Pen</li>
        <li>Pencil</li>
        <li>Eraser</li>
    </ol>

    <!-- Definition list -->
    <dl> 
        <dt> <h4>HTML Definition List</h4> </dt>
        <dd> HTML is easy </dd>
        <dd> HTML is good </dd>
    <dl>


The outputs of above codes are shown in :numref:`fig_html6`, 

.. _`fig_html6`:

.. figure:: fig/html6.png

   Lists 


.. _`sec_links`:

Links
=====


.. code-block:: html

    <!-- links -->
    <p>Go to paragraph with<a href="#para1"> id='para1'</a></p>
    <a href="http://pythondsp.readthedocs.io"> PythonDSP </a>

    <br>
    <p><a href="js.html" target="_self"> JavaScript Tutorial</a> in same window.</p>
    <p><a href="js.html" target="_blank"> JavaScript Tutorial</a> in new Window.</p>

    <p><a href="http://pythondsp.readthedocs.io/pdf">Download PDF, DOC or Zip Files</a></p>


    <p><a href="mailto:pythondsp@gmail.com">Email me</a></p>
    <p><a href="mailto:pythondsp@gmail.com?subject=Feedback&body=Your feedback here">Feeback email</a></p>


.. _`fig_html7`:

.. figure:: fig/html7.png


.. note:: 
    
    We can change the color of the links using 'alink (active link)', 'link' and 'vlink (visited link', by defining these attributes in the 'body tag' as shown below, 

    .. code-block:: html
    
        <body alink="green" link="blue" vlink="red">


Forms
=====


Forms can have different types of controls to collect the input-data from users, which are listed below and shown in :numref:`tbl_ctrl_input`, 

* Text input
* Text area
* Radio button
* Checkbox
* Select box
* File select
* Buttons
* Submit and reset buttons
* Hidden input


.. _`tbl_ctrl_input`:

.. table:: List of control inputs and their attributes 


    +-------------------+------------+--------------------+-------------------------------------------+
    | Control           | Attributes | Values             | Description                               |
    +===================+============+====================+===========================================+
    | Input : text      | type       | text, password     |                                           |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | value      | user-defined       | initial value in the area                 |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | name       | user-defined       | name send to server                       |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | size       | numeric value      | width of the text area                    |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | maxlength  | numeric value      | maximum number of characters              |
    +-------------------+------------+--------------------+-------------------------------------------+
    | Input : radio     | type       | radio              |                                           |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | name       | user-defined       | name send to server                       |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | value      | user-defined value | value of the button if selected           |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | checked    |                    | check the button by default               |
    +-------------------+------------+--------------------+-------------------------------------------+
    | Input : check box | type       | checkbox           |                                           |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | name       | user-defined       | name send to server                       |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | value      | user-defined value | value of the box if selected              |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | checked    |                    | check the box by default                  |
    +-------------------+------------+--------------------+-------------------------------------------+
    | Input : button    | type       | button             | trigger client side script                |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   |            | submit             | submit the form and run 'action'          |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   |            | reset              | reset form                                |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   |            | image              | create image button                       |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | method     | get, post          | get or post method                        |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | action     | user-defined       | action to perform on **submit**           |
    +-------------------+------------+--------------------+-------------------------------------------+
    | Input : hidden    | type       | hidden             | will not display on html, but can be used |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   |            |                    | for sending information to server         |
    +-------------------+------------+--------------------+-------------------------------------------+
    | Selection box     | name       | user-defined       | name send to server                       |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | size       | numeric value      | enables scroll (default dropdown)         |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | multiple   | numeric value      | select multiple items                     |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | value      | user-defined value | value of the item if selected             |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | selected   |                    | select item by default                    |
    +-------------------+------------+--------------------+-------------------------------------------+
    | Text area         | rows, cols | numeric value      | number of rows and cols                   |
    +-------------------+------------+--------------------+-------------------------------------------+
    |                   | name       | user-defined       | name send to server                       |
    +-------------------+------------+--------------------+-------------------------------------------+





* Below are the exmaple of the control inputs described in :numref:`tbl_ctrl_input`

.. code-block:: html

    <!-- Forms -->
    <form>
        <h4>Text input </h4>
        Name : <input type="text" name="user_name" size="4" value="e.g. meher21" maxlength="10"><br>
        Password : <input type="password" name="user_pass" ><br>


        <h4> Radio button: name should be same</h4>
        <input type="radio" name="r_gender"> Male
        <input type="radio" name="r_gender"> Female
        <input type="radio" name="r_gender" checked> Infant

        <h4> Check box : name should be different</h4>
        <input type="checkbox" name="c_male" checked> Male
        <input type="checkbox" name="c_female"> Female
        <input type="checkbox" name="c_infant"> Infant
        
        <h4> Select box : drop-down</h4>
        <select name="s_box">
            <option value="s_male">Male</option>
            <option value="s_female" selected>Female</option>
            <option value="s_infant">Infant</option>
        </select>

        <h4> Select box : scroll</h4>
        <select name="s_box" size="4" multiple>
            <option value="s_male" selected>Male</option>
            <option value="s_female" selected>Female</option>
            <option value="s_infant">Infant 1</option>
            <option value="s_infant" selected>Infant 2</option>
            <option value="s_infant">Infant 3</option>
            <option value="s_infant">Infant 4</option>
        </select>

        <h4> Text area</h4>
        <textarea rows="10" cols="80" name="txt_area">Initial Text
        x = 2 
        y = 3
        </textarea> <!-- formatting work as pre -->

    </form>


:numref:`fig_html8` is the output of above code, 

.. _`fig_html8`:

.. figure:: fig/html8.png

   Various control inputs for creating form



* Below is the code which shows the working of various buttons. Note that **method** and **action** are defined in this form, which will be triggered on 'submit' button. Lastly, 'hidden' option is used in this example. 



.. code-block:: html

    <form method="get|post" action="jquery.html">
        <h4> Buttons and Hidden</h4>
        
        Name : <input type="text" name="user_name" size="4" value="Meher" maxlength="16"><br>
        Password : <input type="password" name="user_pass" ><br>
        
        <input type="button" onclick="alert('Hello')" name="b_alert" value="Say Hello"/><br>
        <input type="submit" name="b_submit" value="Go to jQuery"/>
        <input type="reset" name="b_reset" value="Reset"/><br>
        
        <input type="hidden" name="h_data" value="html_tutorial">
    </form>

:numref:`fig_html9` is the output of above code, 

.. _`fig_html9`:

.. figure:: fig/html9.png

   Various buttons and hidden-input in the form