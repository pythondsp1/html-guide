.. _`ch_bootstrap`:

Bootstrap
*********


Introduction
============

One of the problem with basic HTML design is that the webpage may look different in different browser or device (e.g. mobile, tablet and laptop). Therefore, we may need to modify the code according to browser or device. The problem can be easily resolved by using Bootstrap. 

Bootstrap is a framework which uses HTML, CSS and JavaScript for the web design. It is supported by all the major browsers e.g. Firefox, Opera and Chrome etc. Further, Bootstrap includes several predefined classes for easy layouts e.g. dropdown buttons, navigation bar and alerts etc. Lastly, it is responsive in nature i.e. the layout changes automatically according to the device e.g. mobile or laptop etc. 


Setup
=====

Bootstrap needs atleast 3 files for its operation which can be `downloaded from the Bootstrap website <http://getbootstrap.com/docs/3.3/getting-started/#download>`_.

* **bootstrap.css (Line 7)** : This file contains various CSS for bootstrap. 
* **bootstrap.js (Line 16)** : This file contains various JavaScript functionalities e.g. dropdown and alerts etc.
* **jQuery.js (Line 17)** : This file is the jQuery library which can be downloaded from the 'jQuery' website. It is required for proper working of 'bootstrap.js'. 


Download and include files
--------------------------

These files are downloaded and saved inside the 'asset' folder. Next, we need to include these files in the HTML document as below, 

.. code-block:: html
    :linenos:
    :emphasize-lines: 7, 16, 17

    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom CSS below -->

    </head>
    <body>


        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
    </html>



Add CDN
-------

Another way to include the files is CDN. In this method, we need not to download the files, but provide the links to these files, as shown in Lines 8, 17 and 19 of below code. Note that, in this case the code will not work in offline mode. 


.. code-block:: html
    :linenos:
    :emphasize-lines: 8, 17, 19

    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>

        <!-- CSS -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Add Custom CSS below -->

    </head>
    <body>


        <!-- Javascript -->
        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </body>
    </html>


.. note:: 

    In this tutorial, we have used the first method i.e. download the files and include in the html document. 


Check setup
-----------

Let's write our first code to check the correct setup of the Bootstrap. 

.. note:: 

    We need not to memorize the code for creating the 'dropdown' button. All the templates are available on the `Bootstrap website <https://getbootstrap.com/docs/3.3/components/>`_. Copy and paste the code from there and the modify the code according to need as shown in this tutorial. 

.. code-block:: html
    :linenos: 
    :emphasize-lines: 13-25

    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom CSS below -->

    </head>
    <body>

        <div class="dropdown">
          <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Dropdown
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </div>

        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
    </html>

The resultant web page is shown in :numref:`fig_bootstrap1`, 

.. _`fig_bootstrap1`:

.. figure:: fig/bootstrap1.png

   Click on the 'Dropdown' button


Grid system
===========

Bootstrap divides the **each row** into **12 columns**. Then following commands can be used to specify the width the columns

* **col-lg-4** : It will select 4 columns. Choose any number between 1-12. The 'lg' stand for **large screen** (e.g. large computer screen).
* **col-md-5** : 'md' = medium screen 
* **col-sm-3** : 'sm' = small screen
* **col-xs-3** : 'xs' = extra small screen
* col-lg-**offset**-4 : skip first 4 columns. Simimlary use 'md', 'sm' and 'xs' with offset

Example
-------

Below is an example of grid system. Read the content of the Lines 13-16 to understand it. The resultant webpage is shown in :numref:`fig_bootstrap2`. 

.. note:: 

    * For easy visualization, in the below code the CSS code (Lines 9-21) is used to fill the columns with colors and border.
    * The columns (Lines 28-30) should be defined inside the class 'row' (Line 27).
    * Also, sum of the widths of individual columns should not be greater than 12.
    * Lastly, if we use 'col-md-4' without defining the 'lg', 'sm' and 'xs', then 'md' rule will be applicable to higher size screen 'lg', but **not** on the lower size screen 'sm' and 'xs'. Similary, if we use 'sm' without defining the 'lg', 'md' and 'xs', then rule will be applicable to higher size screen i.e. 'lg' and 'md' but not on 'xs'

.. code-block:: html
    :linenos:
    :emphasize-lines: 9-21, 27-31

    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom CSS below -->
        <style type="text/css">
            .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8,
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8,
            .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8 {
                background-color: green;
                color: white;
                font-weight: bold;
                border: 1px solid red;
                height: 3em;  /*height of the box*/
                text-align: center; /*vertical center the text*/
            }
        </style>

    </head>

    <body>

        <div class="row">
            <div class="col-md-2 col-xs-4">col-md-2, col-xs-4</div>
            <div class="col-md-6 col-xs-4">col-md-6, col-xs-4</div>
            <div class="col-md-4 col-xs-4">col-md-4, col-xs-4</div>
        </div>


        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
    </html>


.. _`fig_bootstrap2`:

.. figure:: fig/bootstrap2.png

   Different grid-size (i.e. 2, 6, 4) for 'medium' and 'large' screen

.. _`fig_bootstrap3`:

.. figure:: fig/bootstrap3.png

   Equal grid-size (i.e. 4) for 'extra small' and 'small' screen



Nested columns
--------------

We can further divide a column into small columns by defining a class 'row (Line 30)' inside the 'column' (Line 29), as shown in Lines 29-34.

.. code-block:: html
    :linenos:
    :emphasize-lines: 29-34

    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom CSS below -->
        <style type="text/css">
            .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8,
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8,
            .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8 {
                background-color: green;
                color: white;
                font-weight: bold;
                border: 1px solid red;
                height: 3em;  /*height of the box*/
                text-align: center; /*vertical center the text*/
            }
        </style>

    </head>

    <body>

        <div class="row">
            <div class="col-md-2 col-xs-4">col-md-2, col-xs-4</div>
            <div class="col-md-6 col-xs-4">
                <div class="row">
                    <div class="col-xs-6">col-xs-6</div>
                    <div class="col-xs-6">col-xs-6</div>
                </div>
            </div>
            <div class="col-md-4 col-xs-4">col-md-4, col-xs-4</div>
        </div>


        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
    </html>


Offset
------

We can skip the columns using 'offset' as shown in Line 27; and the corresponding html page is shown in :numref:`fig_bootstrap4`, 

.. code-block:: html
    :linenos:
    :emphasize-lines: 27


    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>

        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom CSS below -->
        <style type="text/css">
            .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8,
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8,
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8,
            .col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8 {
                background-color: green;
                color: white;
                font-weight: bold;
                border: 1px solid red;
                height: 3em;  /*height of the box*/
                text-align: center; /*vertical center the text*/
            }
        </style>
    </head>

    <body>

        <div class="row">
            <div class="col-md-offset-2 col-md-2">col-md-2</div>
            <div class="col-md-8">col-md-8</div>
        </div>


        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
    </html>


.. _`fig_bootstrap4`:

.. figure:: fig/bootstrap4.png

   Offset


Components
==========

Once we understand the 'html', 'css' and 'Bootstrap-grid' understood, then the use of Bootstrap-components are straightforward. Just copy and paste the code from the `Bootstrap website <https://getbootstrap.com/docs/3.3/components/>`_ and modify it according to the requirement. 

In this section, we will use following template.

.. code-block:: html

    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>
        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom CSS below -->
    </head>

    <body>



        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
    </html>


Labels
------

* The class 'label' is used to create the label (Line 11).
* The class 'label-info' sets the color of the label to 'blue' (Line 11). 
* The size of the 'labels' can be changes using 'element' tags (e.g. 'h3' in Line 11) outside the 'span' tag (Line 11). 

.. note:: 

    Bootstrap provides 6 color option, 

    * danger
    * default
    * info
    * primary
    * success
    * warning

.. code-block:: html
    :linenos:
    :emphasize-lines: 11


    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>
        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom CSS below -->
    </head>

    <body>
        <h3>Example heading <span class="label label-info">New</span></h3>
        <hr>

        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
    </html>


* :numref:`fig_bootstrap5` is the output of above code, 

.. _`fig_bootstrap5`:

.. figure:: fig/bootstrap5.png

   Label


.. note:: 

    All the codes will be added in the 'body' tag, and the new code will be inserted below the previous codes. Therefore full HTML code is not added in the tutorial. 





Buttons
-------

The element 'button' can be used to create the button in Bootstrap as shown below, 

.. note:: 

    * We can use 'lg', 'md' and 'xs' etc. to set the size of the button. 
    * In Line 3, a glyphicon is added (i.e. trash sign). More glyphicon can be found on the `Boostrap-webpage <https://getbootstrap.com/docs/3.3/components/#glyphicons>`_. 

.. code-block:: html

    <h3> Buttons </h3>
    <button type="button" class="btn btn-primary">Sign in</button>
    <button type="button" class="btn btn-danger"> Delete <span class="glyphicon glyphicon-trash"><span></button>
    <button type="submit" class="btn btn-lg btn-success">Submit</button>
    <button type="submit" class="btn btn-md btn-success">Submit</button>
    <button type="submit" class="btn btn-sm btn-success">Submit</button>


* :numref:`fig_bootstrap6` is the output of above code, 

.. _`fig_bootstrap6`:

.. figure:: fig/bootstrap6.png

   Buttons


Forms
-----

In this section, 4 types of forms are added, 

Basic form
^^^^^^^^^^

Output of below code is shown in :numref:`fig_bootstrap7`, 

.. code-block:: html

    <h2> Basic form </h2>
    <div class="row">
    <div class="col-sm-5" style="background: pink">
    <div class="panel-body">
        <form role="form">
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input class="form-control" id="exampleInputEmail1" placeholder="Enter email" type="email">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input class="form-control" id="exampleInputPassword1" placeholder="Password" type="password">
            </div>
            <div class="form-group">
                <label for="exampleInputFile">File input</label>
                <input id="exampleInputFile" type="file">
                <p class="help-block">Example block-level help text here.</p>
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Check me out
                </label>
            </div>
            <button type="submit" class="btn btn-info">Submit</button>
        </form>
    </div>
    </div>
    </div>
    <br><br>

.. _`fig_bootstrap7`:

.. figure:: fig/bootstrap7.png

   Basic form

Horizontal form
---------------

Output of below code is shown in :numref:`fig_bootstrap8`, 

.. code-block:: html

    <h3> Horizontal form </h3>
    <div class="row">
    <div class="col-sm-5" style="background: pink">
    <div class="panel-body">
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label for="inputEmail1" class="col-lg-2 col-sm-2 control-label">Email</label>
                <div class="col-lg-10">
                    <input class="form-control" id="inputEmail1" placeholder="Email" type="email">
                    <p class="help-block">Example block-level help text here.</p>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword1" class="col-lg-2 col-sm-2 control-label">Password</label>
                <div class="col-lg-10">
                    <input class="form-control" id="inputPassword1" placeholder="Password" type="password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"> Remember me
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" class="btn btn-danger">Sign in</button>
                </div>
            </div>
        </form>
    </div>
    </div>
    </div>

.. _`fig_bootstrap8`:

.. figure:: fig/bootstrap8.png

   Horizontal form

Inline form
^^^^^^^^^^^

Output of below code is shown in :numref:`fig_bootstrap9`, 


.. code-block:: html

    <h2> Inline form </h2>
    <div class="row">
    <div class="col-sm-5" style="background: pink">
    <div class="panel-body">
        <form class="form-inline" role="form">
            <div class="form-group">
                <label class="sr-only" for="exampleInputEmail2">Email address</label>
                <input class="form-control" id="exampleInputEmail2" placeholder="Enter email" type="email">
            </div>
            <div class="form-group">
                <label class="sr-only" for="exampleInputPassword2">Password</label>
                <input class="form-control" id="exampleInputPassword2" placeholder="Password" type="password">
            </div>
            <div class="checkbox">
                <label>
                    <input type="checkbox"> Remember me
                </label>
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
        </form>
    </div>
    </div>
    </div>
    <br><br>

.. _`fig_bootstrap9`:

.. figure:: fig/bootstrap9.png

   Inline form


Modal form
^^^^^^^^^^

Modal form appears in the pop-up window. Output of below code is shown in :numref:`fig_bootstrap10`, 


.. code-block:: html

    <!-- model form -->
    <h3> Modal form </h3>

    <div class="row">
    <div class="col-sm-5" style="background: pink">
    <div class="panel-body">
        <!-- button to generate model form -->
        <a href="#myModal" data-toggle="modal" class="btn btn-xs btn-success">
            Form in Modal
        </a>

        <!-- model form settings-->

        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
                        <h4 class="modal-title">Form Tittle</h4>
                    </div>
                    <div class="modal-body">

        <!-- actual form  -->
                        <form role="form">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email address</label>
                                <input class="form-control" id="exampleInputEmail3" placeholder="Enter email" type="email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password</label>
                                <input class="form-control" id="exampleInputPassword3" placeholder="Password" type="password">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">File input</label>
                                <input id="exampleInputFile3" type="file">
                                <p class="help-block">Example block-level help text here.</p>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Check me out
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </form>
        <!-- actual form ends -->

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <hr>

.. _`fig_bootstrap10`:

.. figure:: fig/bootstrap10.png

   Modal form

Form elements
-------------

Output of below code is shown in :numref:`fig_bootstrap11`, 


.. code-block:: html

    <!-- Form elements  -->
        <h3> Form elements </h3>
        <div class="row">
        <div class="col-sm-5" style="background: pink">
        <div class="panel-body">
            <form class="form-horizontal tasi-form" method="get">
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Default</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Help text</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="text">
                        <span class="help-block">A block of help text that breaks onto a new line and may extend beyond one line.</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Rounder</label>
                    <div class="col-sm-10">
                        <input class="form-control round-input" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Input focus</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="focusedInput" value="This is focused..." type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Disabled</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="disabledInput" placeholder="Disabled input here..." disabled="disabled" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Placeholder</label>
                    <div class="col-sm-10">
                        <input class="form-control" placeholder="placeholder" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                        <input class="form-control" placeholder="" type="password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-2 col-sm-2 control-label">Static control</label>
                    <div class="col-lg-10">
                        <p class="form-control-static">email@example.com</p>
                    </div>
                </div>
            </form>
        </div>
        </div>
        </div>

.. _`fig_bootstrap11`:

.. figure:: fig/bootstrap11.png

   Form elements



Control size
------------

Output of below code is shown in :numref:`fig_bootstrap12`, 


.. code-block:: html

    <!-- Control size -->
    <h3>Control size</h3>
    <div class="row">
    <div class="col-sm-5" style="background: pink">
    <div class="panel-body">
        <form class="form-horizontal tasi-form" method="get">
            <div class="form-group">
                <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Control sizing</label>
                <div class="col-lg-10">
                    <input class="form-control input-lg m-bot15" placeholder=".input-lg" type="text">
                    <input class="form-control m-bot15" placeholder="Default input" type="text">
                    <input class="form-control input-sm m-bot15" placeholder=".input-sm" type="text">

                    <select class="form-control input-lg m-bot15">
                        <option selected="selected">Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                    </select>
                    <select class="form-control m-bot15">
                        <option selected="selected">Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                    </select>
                    <select class="form-control input-sm m-bot15">
                        <option selected="selected">Option 1</option>
                        <option>Option 2</option>
                        <option>Option 3</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    </div>


.. _`fig_bootstrap12`:

.. figure:: fig/bootstrap12.png

   Form elements



More buttons
------------

Output of below code is shown in :numref:`fig_bootstrap13`, 


.. code-block:: html

    <h3>Buttons</h3>
        <div class="row">
        <div class="col-sm-12" style="background: pink">
            <div class="panel-body">
                <form class="form-horizontal tasi-form" method="get">
                    <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Checkboxes and radios</label>
                        <div class="col-lg-10">
                            <div class="checkbox">
                                <label>
                                    <input value="" type="checkbox">
                                    Option one is this and that—be sure to include why it's great
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input value="" type="checkbox">
                                    Option one is this and that—be sure to include why it's great option one
                                </label>
                            </div>

                            <div class="radio">
                                <label>
                                    <input name="optionsRadios" id="optionsRadios1" value="option1" checked="checked" type="radio">
                                    Option one is this and that—be sure to include why it's great
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input name="optionsRadios" id="optionsRadios2" value="option2" type="radio">
                                    Option two can be something else and selecting it will deselect option one
                                </label>
                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Inline checkboxes</label>
                        <div class="col-lg-10">
                            <label class="checkbox-inline">
                                <input id="inlineCheckbox1" value="option1" type="checkbox"> 1
                            </label>
                            <label class="checkbox-inline">
                                <input id="inlineCheckbox2" value="option2" type="checkbox"> 2
                            </label>
                            <label class="checkbox-inline">
                                <input id="inlineCheckbox3" value="option3" type="checkbox"> 3
                            </label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Selects</label>
                        <div class="col-lg-10">
                            <select id="select-dropdown-num" class="form-control m-bot15">
                                <option selected="selected">1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>

                            <select multiple="multiple" class="form-control">
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Column sizing</label>
                        <div class="col-lg-10">
                            <div class="row">
                                <div class="col-lg-2">
                                    <input class="form-control" placeholder=".col-lg-2" type="text">
                                </div>
                                <div class="col-lg-3">
                                    <input class="form-control" placeholder=".col-lg-3" type="text">
                                </div>
                                <div class="col-lg-4">
                                    <input class="form-control" placeholder=".col-lg-4" type="text">
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
            </div>
            </div>

.. _`fig_bootstrap13`:

.. figure:: fig/bootstrap13.png

   Buttons


Input group
-----------

Output of below code is shown in :numref:`fig_bootstrap14`,


.. code-block:: html

    <!-- Input groups -->
    <h3>Input groups</h3>
    <div class="row">
    <div class="col-sm-12" style="background: pink">
    <div class="panel-body">
        <form class="form-horizontal tasi-form" method="get">
            <div class="form-group">
                <label class="col-sm-2 control-label col-lg-2">Basic examples</label>
                <div class="col-lg-10">
                    <div class="input-group m-bot15">
                        <span class="input-group-addon">@</span>
                        <input class="form-control" placeholder="Username" type="text">
                    </div>

                    <div class="input-group m-bot15">
                        <input class="form-control" type="text">
                        <span class="input-group-addon">.00</span>
                    </div>

                    <div class="input-group m-bot15">
                        <span class="input-group-addon">$</span>
                        <input class="form-control" type="text">
                        <span class="input-group-addon">.00</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label col-lg-2">Sizing</label>
                <div class="col-lg-10">
                    <div class="input-group input-group-lg m-bot15">
                        <span class="input-group-addon">@</span>
                        <input class="form-control input-lg" placeholder="Username" type="text">
                    </div>

                    <div class="input-group m-bot15">
                        <span class="input-group-addon">@</span>
                        <input class="form-control" placeholder="Username" type="text">
                    </div>

                    <div class="input-group input-group-sm m-bot15">
                        <span class="input-group-addon">@</span>
                        <input class="form-control" placeholder="Username" type="text">
                    </div>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label col-lg-2">Checkboxe and radio</label>
                <div class="col-lg-10">
                    <div class="input-group m-bot15">
                        <span class="input-group-addon">
                            <input type="checkbox">
                        </span>
                        <input class="form-control" type="text">
                    </div>

                    <div class="input-group m-bot15">
                        <span class="input-group-addon">
                            <input type="radio">
                        </span>
                        <input class="form-control" type="text">
                    </div>

                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label col-lg-2">Button addons</label>
                <div class="col-lg-10">
                    <div class="input-group m-bot15">
                        <span class="input-group-btn">
                            <button class="btn btn-white" type="button">Go!</button>
                        </span>
                        <input class="form-control" type="text">
                    </div>

                    <div class="input-group m-bot15">
                        <input class="form-control" type="text">
                        <span class="input-group-btn">
                            <button class="btn btn-white" type="button">Go!</button>
                        </span>
                    </div>

                    <div class="input-group m-bot15">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <input class="form-control" type="text">
                    </div>
                    <div class="input-group m-bot15">
                        <input class="form-control" type="text">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-white dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label col-lg-2">Segmented buttons</label>
                <div class="col-lg-10">
                    <div class="input-group m-bot15">
                        <div class="input-group-btn">
                            <button tabindex="-1" class="btn btn-white" type="button">Action</button>
                            <button tabindex="-1" data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">
                                <span class="caret"></span>
                            </button>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <input class="form-control" type="text">
                    </div>

                    <div class="input-group m-bot15">
                        <input class="form-control" type="text">
                        <div class="input-group-btn">
                            <button tabindex="-1" class="btn btn-white" type="button">Action</button>
                            <button tabindex="-1" data-toggle="dropdown" class="btn btn-white dropdown-toggle" type="button">
                                <span class="caret"></span>
                            </button>
                            <ul role="menu" class="dropdown-menu pull-right">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    </div>
    </div>


.. _`fig_bootstrap14`:

.. figure:: fig/bootstrap14.png

   Input groups



Navigation bar (navbar)
-----------------------

For navbar, one more 'css' file is added at Line 8. This CSS will create the space at the top for the 'navbar' so that it will not overlap with the contents of the HTML page. Further, Lines 13-44 adds various links to the navbar. Lastly, we can add HTML content below Line 46. 

Output of below code is shown in :numref:`fig_bootstrap15`,

.. code-block:: html
    :linenos:
    :emphasize-lines: 8, 13-44

    <!DOCTYPE html>
    <html>
    <head>
        <title>Bootstrap Tutorial</title>
        <!-- CSS -->
        <link href="asset/css/bootstrap.min.css" rel="stylesheet">
        <!-- Add Custom CSS below -->
        <link href="asset/css/theme.css" rel="stylesheet">  <!-- make space at the top for nav-bar -->
    </head>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand hidden-sm" href="http://pythondsp.readthedocs.io/">PythonDSP</a>
            </div>
            <div class="row">
              <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">

                  <li><a href="basic.html">HTML</a></li>
                  <li><a href="css.html">CSS</a></li>
                  
           
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">More<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="bootstrap.html">Bootstrap</a></li>
                      <li><a href="js.html">Javascript</a></li>
                      <li><a href="jquery.html">jQuery</a></li>
                    </ul>
                  </li>

                </ul>
                </div>
            </div><!--/.nav-collapse -->
          </div>
        </div>

        <!-- other codes here -->

        <!-- Javascript -->
        <!-- put jQuery.js before bootstrap.min.js; and then add custom jquery-->
        <script src="asset/js/jquery-3.3.1.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
    </body>
    </html>



.. _`fig_bootstrap15`:

.. figure:: fig/bootstrap15.png

   Navigation bar (navbar)